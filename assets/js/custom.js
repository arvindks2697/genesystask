$(".add-item").on("click",function(){
    var addto = $(".add-todo");
    addto.toggleClass("show");
    if(addto.hasClass("show")){
        addto.fadeIn("show");
    }else{
        addto.fadeOut("show");
    }
    $(".add-todo").val("");
})
$(".clear-list").on("click",function(){
    var emptyarr =[];
    addToLocalStorage(emptyarr);
    $(".add-todo").val("");
    loadListItems();
})
$(document).on("click",".removeItem", function (e) {
  var existingListItems = JSON.parse(localStorage.getItem("listitems"));
  var parent = $(this).parent();
  var id = parent.attr("data-index");
  existingListItems.splice(id, 1);
  addToLocalStorage(existingListItems)
  loadListItems();
  e.stopPropagation();
});
$(document).on("click",".todolist li", function () {
  var existingListItems = JSON.parse(localStorage.getItem("listitems"));
  var id = $(this).attr("data-name");
//   existingListItems.find(item => item.name === id).checked = true;
  existingListItems.map((item,index)=>{
      if(item.name === id){
          item.checked = true;
      }
  })
  addToLocalStorage(existingListItems)
  $(this).toggleClass("completed")
});
$(".add-todo").on("keyup", function (e) {
  var pressedKey = e.which;
  var existingListItems = localStorage.getItem("listitems");
  var listItems = [];
  if (pressedKey === 13) {
    if (existingListItems) {
      existingListItems = JSON.parse(existingListItems);
      existingListItems.push({
        name: e.target.value,
        checked: false,
      });
      addToLocalStorage(existingListItems)
    } else {
      listItems.push({
        name: e.target.value,
        checked: false,
      });
      addToLocalStorage(listItems)
    }
    loadListItems();
    $(this).val("");
  }
});
function loadListItems() {
  var existingListItems = localStorage.getItem("listitems");
  var template = "";
  if (existingListItems) {
    existingListItems = JSON.parse(existingListItems);
    existingListItems.map((item, index) => {
      if (item.checked === true) {
        template += `<li data-index="${index}" class="completed" data-name="${item.name}"><span class="removeItem"><i class="far fa-trash-alt"></i></span>${item.name}</li>`;
      } else {
        template += `<li data-index="${index}" data-name="${item.name}"><span class="removeItem"><i class="far fa-trash-alt"></i></span>${item.name}</li>`;
      }
    });
    $(".todolist").fadeOut(200, function () {
      $(this).html("");
    });
    $(".todolist").fadeIn(400, function () {
      $(this).html(template);
    });
  }
}
$(document).ready(function () {
  loadListItems();
  $(".add-todo").hide();
});
function addToLocalStorage(data){
    localStorage.setItem("listitems", JSON.stringify(data));
}